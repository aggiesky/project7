insert into DISNEY_CHARACTER
(ID, FIRST_NAME, LAST_NAME, YEAR_INTRODUCED, TALKS, TEMPERAMENT, GENDER)
values
(1, 'Mickey', 'Mouse', 1928, true, 'friendly', 'm'),
(2, 'Minnie', 'Mouse', 1928, true, 'friendly', 'f'),
(3, 'Donald', 'Duck', 1934, true, 'grumpy', 'm'),
(4, 'Daisy', 'Duck', 1940, true, 'friendly', 'f'),
(5, 'Pluto', '', 1930, false, 'friendly', 'm');

