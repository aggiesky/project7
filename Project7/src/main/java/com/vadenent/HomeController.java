package com.vadenent;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;

@Controller
@RequestMapping("/")
public class HomeController {

	
	@Autowired
	DisneyCharacterRepository characterRepository;
	
	
	
	@GetMapping("/")
	public String masterView(Model model) {
		
		model.addAttribute("characters", characterRepository.findAll());
		return "master";
	} 
	
	
	
	@GetMapping("/details/{id}")
	public String detailsView(Model model, @PathVariable Long id) {
		
		Optional<DisneyCharacter> disneyCharacter = characterRepository.findById(id);
		
		if (disneyCharacter.isPresent()) {
			
			System.out.println(">>> disneyCharacter:: " + disneyCharacter);
			
			model.addAttribute("characters", disneyCharacter.get());
			return "details";
		} else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	
	}  // end method detailsView
	
}
