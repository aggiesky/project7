package com.vadenent;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class DisneyCharacter {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private String firstName;
	private String lastName;
	private int yearIntroduced;
	private boolean talks;
	private String temperament;
	private char gender;

	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getYearIntroduced() {
		return yearIntroduced;
	}
	public void setAge(int yearIntroduced) {
		this.yearIntroduced = yearIntroduced;
	}

	public boolean isTalks() {
		return talks;
	}
	public void setTalks(boolean talks) {
		this.talks = talks;
	}

	public String getTemperament() {
		return temperament;
	}
	public void setTemperament(String temperament) {
		this.temperament = temperament;
	}

	public char getGender() {
		return gender;
	}
	public void setGender(char gender) {
		this.gender = gender;
	}
	public boolean isMale() {
		return (gender == 'm' || gender == 'M');
	}
	public boolean isFemale() {
		return (gender == 'f' || gender == 'F');
	}


	@Override
	public String toString() {
		return "CartoonCharacters [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", yearIntroduced=" 
				+ yearIntroduced + ", talks=" + talks + ", temperament=" + temperament + ", gender=" + gender + "]";
	}
	
	
}
