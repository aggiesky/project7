package com.vadenent;

import org.springframework.data.repository.CrudRepository;

public interface DisneyCharacterRepository 
		extends CrudRepository<DisneyCharacter, Long> {

}
